from scripts.handler.color_finder_handler import unique_colors
from scripts.handler.common_keys_values import common_data
from flask import Blueprint, request

color_finder = Blueprint("color_finder_blueprint", __name__)


@color_finder.route('/postjson', methods=['POST'])
def postJsonHandler():
    content = request.get_json()
    colors = unique_colors(content, [])
    return str(colors)
@color_finder.route('/post_json_data', methods=['POST'])
def postJsonHandlerProgram():
    content = request.get_json()
    data = common_data(content)
    return str(data)
