def unique_colors(data, lst_colors):
    if not isinstance(data, str):
        for k, v in data.items():
            if isinstance(v, dict):
                unique_colors(v, lst_colors)
            elif hasattr(v, '__iter__') and not isinstance(v, str):
                for dictitem in v:
                    unique_colors(dictitem, lst_colors)
            elif isinstance(v, str):
                if k == "color":
                    lst_colors.append(v)
            else:
                if k == "color":
                    lst_colors.append(v)
    return (set(lst_colors))
