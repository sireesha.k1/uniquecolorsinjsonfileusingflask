dic_data = dict()
i = 0


def common_data(data):
    global i
    if not isinstance(data, str):
        for k, v in data.items():
            if isinstance(v, dict):
                common_data(v)
            elif hasattr(v, '__iter__') and not isinstance(v, str):
                for dictitem in v:
                    common_data(dictitem)
            elif isinstance(v, str):
                if k == "label":
                    i += 1
                    dic_data["type" + str(i)] = v
            else:
                if k == "label":
                    i += 1
                    dic_data["type" + str(i)] = v

    return (dic_data)
